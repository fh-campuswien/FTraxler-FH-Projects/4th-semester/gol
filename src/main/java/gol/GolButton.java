package gol;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class GolButton extends JButton implements Observer
{
    @Override
    public void update(Observable o, Object arg)
    {
        this.setBackground(((Cell)o).getState() ? Color.GREEN : Color.white);
    }
}
